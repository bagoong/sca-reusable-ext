define(
	'Kodella.KDNoIndexNoFollow.KDNoIndexNoFollow'
,   [
		'SC.Configuration',
		'jQuery'
	]
,   function (
		Configuration,
		jQuery
	)
{
	'use strict';

	return  {
		mountToApp: function mountToApp (container)
		{

			/** @type {LayoutComponent} */
			var layout = container.getComponent('Layout');

			if(layout)
			{
				var $head;
        var $meta;
				var enableFeature = Configuration.get('kdlnoindexnofollow.enable');
        if (enableFeature) {
            $head = jQuery('head');
            $meta = $head.find('meta[name="robots"]');
            if ($meta.length > 0) {
                $meta.attr('content', 'noindex, nofollow');
            } else {
                $meta = jQuery('<meta name="robots" content="noindex, nofollow">');
                $head.append($meta);
            }
        }
			}
		}
	};
});
