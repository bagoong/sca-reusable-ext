define('Cart.Confirmation.Helpers.Ext'
,	[
		'Cart.Confirmation.Helpers'

	,	'Utils'
	,	'jQuery'
	,	'underscore'

	]
,	function
	(
		CartConfirmationHelpers

	,	Utils
	,	jQuery
	,	_

	)
{
	'use strict';
	return {

  		loadModule: function loadModule(options) {
	    	CartConfirmationHelpers._showCartConfirmationModal = function _showCartConfirmationModal (cart_promise, line, application){
				cart_promise.done(function () {
					jQuery("[data-view='Header.MiniCart']").addClass("open");
				});
			}
		}


	}
});
