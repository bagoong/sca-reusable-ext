define(
	'Header.MiniCart.View.Extd'
,	[
		'Header.MiniCart.View',
		'LiveOrder.Model'
	,	'jQuery'
	,	'underscore'
	]
,	function (
		View
	,	LiveOrderModel
	,	jQuery
	,	_
	)
{
	'use strict';

	return {
        
        loadModule: function loadModule(options) {
			_.extend(View.prototype, {	  
				events: _.extend(View.prototype.events, {
					'click .removeThisItem': 'removeThisItem'
				}),
				removeThisItem: function(e) {
					e.stopPropagation();
		
					var self = this;
					var itemLines = LiveOrderModel.getInstance().get('lines');
					var selectedItem = jQuery(e.target).closest('.kd-cart-cell').attr('data-item-id');
		
					var selectedItemLine = _.find(itemLines.models, function(line) {
						return line.get('item').id == selectedItem;
					});
		
					var remove_promise = this.model.removeLine(selectedItemLine);
		
					this.isRemoving;
		
					remove_promise.always(function () {
						self.isRemoving = false;
					});
		
					return remove_promise;
				}
			});
		}

	}

});
