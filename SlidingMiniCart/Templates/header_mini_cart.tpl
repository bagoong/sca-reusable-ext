<a class="header-mini-cart-menu-cart-link {{#if showLines}}header-mini-cart-menu-cart-link-enabled{{/if}}" data-type="mini-cart" title="{{translate 'Cart'}}" data-touchpoint="{{cartTouchPoint}}" data-hashtag="#cart" href="#">
	<i class="header-mini-cart-menu-cart-icon"></i>
	<span class="header-mini-cart-menu-cart-legend">
		{{#if isLoading}}
		<span class="header-mini-cart-summary-cart-ellipsis"></span>
		{{else}}
			{{#if showPluraLabel}}
				{{translate '$(0) items' itemsInCart}}
			{{else}}
				{{translate '1 item'}}
			{{/if}}
		{{/if}}
	</span>
</a>

<div class="header-mini-cart">
	<!-- Mini Cart Header -->
	<div class="header-mini-cart-top">
	        <span class="header-mini-cart-top-left">Cart <span>{{translate '$(0)' itemsInCart}}</span></span>
	        <span class="header-mini-cart-top-right kd-heading-1">CLOSE</span>
	</div>

	<!-- Mini Cart Content -->
	{{#if showLines}}
	 	<div class="header-mini-cart-container">
	 		<div data-view="Header.MiniCartItemCell"></div>
		</div>

		{{else}}
		<div class="header-mini-cart-empty">
			<a href="#" data-touchpoint="{{cartTouchPoint}}" data-hashtag="#cart">
				{{#if isLoading}}
					{{translate 'Your cart is loading'}}
				{{else}}
					{{translate 'Your cart is empty'}}
				{{/if}}
			</a>
		</div>
	{{/if}}

	<!-- Mini Cart Footer -->
	<div class="kd-mini-cart-bottom">
		{{#if showLines}}
			<div class="header-mini-cart-subtotal">
				<div class="header-mini-cart-subtotal-items">
					{{#if showPluraLabel}}
						{{translate '$(0) items' itemsInCart}}
					{{else}}
						{{translate '1 item'}}
					{{/if}}
				</div>

				{{#if isPriceEnabled}}
				<div class="header-mini-cart-subtotal-amount">
					{{translate 'SUBTOTAL: $(0)' subtotalFormatted}}
				</div>
				{{/if}}
			</div>
			<div class="header-mini-cart-buttons">
				<div class="header-mini-cart-buttons-left">
					<a href="#" class="header-mini-cart-button-view-cart" data-touchpoint="{{cartTouchPoint}}" data-hashtag="#cart"  data-action="view-cart">
						{{translate 'View Cart'}}
					</a>
				</div>
				<div class="header-mini-cart-buttons-right">
					<a href="#" class="header-mini-cart-button-checkout" data-touchpoint="checkout" data-hashtag="#"  data-action="checkout">
						{{translate 'Checkout'}}
					</a>
				</div>
			</div>
		{{else}}
			<a href="#search" class="header-mini-cart-button-checkout" data-touchpoint="home" data-hashtag="#search" >START SHOPPING</a>
		{{/if}}
	</div>

</div>