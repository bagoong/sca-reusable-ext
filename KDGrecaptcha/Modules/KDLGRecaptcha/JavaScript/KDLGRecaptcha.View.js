// @module Kodella.KDLGRecaptcha.KDLGRecaptcha
define('Kodella.KDLGRecaptcha.KDLGRecaptcha.View'
,	[
		'kodella_kdlgrecaptcha_kdlgrecaptcha.tpl'
	,	'Utils'
	,	'Backbone'
	,	'jQuery'
	,	'underscore'
	,	'Kodella.KDLGRecaptcha.BackboneFormViewExt.View'
	,	'SC.Configuration'
	,	'Backbone.FormView'
	]
,	function (
		kodella_kdlgrecaptcha_kdlgrecaptcha_tpl
	,	Utils
	,	Backbone
	,	jQuery
	,	_
	,	BackboneFormViewExt
	,	Configuration
	,	BackboneFormView
	)
{
	'use strict';
	
	// @class Kodella.KDLGRecaptcha.KDLGRecaptcha.View @extends Backbone.View
	return Backbone.View.extend({

		template: kodella_kdlgrecaptcha_kdlgrecaptcha_tpl

	,	initialize: function (options) {
		
		}
	
	,	events: {
			
		}

	,	bindings: {
		}

	, 	childViews: {
			
		}
	
		//@method getContext @return Kodella.KDLGRecaptcha.KDLGRecaptcha.View.Context
	,	getContext: function getContext()
		{
			
			
			//@class Kodella.KDLGRecaptcha.KDLGRecaptcha.View.Context
			return {
				sitekey:Configuration.get('kodellaApp.sitekey').trim(),
				useCaptcha:Configuration.get('kodellaApp.enableCaptcha')

			};
		}
	});
});