// @module Kodella.KDSystemError.KDSystemError
define('Kodella.KDLGRecaptcha.BackboneFormViewExt.View'
,	[
		'Utils'
	,	'Backbone'
	,	'jQuery'
	,	'underscore'
	,	'Backbone.FormView'
	,	'SC.Configuration'
	]
,	function (
		Utils
	,	Backbone
	,	jQuery
	,	_
	,	BackboneFormView
	,	Configuration
	)
{
	'use strict';
	

	function buttonSubmitDone (savingForm)
	{
		savingForm.find('[type="submit"]').each(function()
		{
			var element = jQuery(this);
			element.attr('disabled', false);
			element.text(element.data('default-text'));
		});
	}
	// override default handler to uncheck radio buttons.
	var handler_radio = _.findWhere(Backbone.Stickit._handlers, {'selector': 'input[type="radio"]'});

	if (handler_radio)
	{
		handler_radio.update = function ($el, val)
		{
			if (val)
			{
				$el.filter('[value="'+val+'"]').prop('checked', true);
			}
			else
			{
				$el.prop('checked', false);
			}
		};
	}

	// @class Kodella.KDSystemError.KDSystemError.View @extends Backbone.View
	return {
		loadModule:function loadModule(options){
            //console.log('extension bui',BackboneFormView);
            var _self = this;
           // console.log('selfie',self);
			_.extend(BackboneFormView  || {},{
				saveForm :function (e, model, props)
				{
                    //console.log('dis',this);
					e.preventDefault();
                    var captchaContainer = $(e.target).find('#captcha-container');
                    //Add validate method into the view.model
                    Backbone.Validation.bind(this);

                    model = model || this.model;

                    this.$savingForm = jQuery(e.target).closest('form');
                    this.isSavingForm = true;

                    if (this.$savingForm.length)
                    {
                        // and hides reset buttons
                        this.$savingForm.find('input[type="reset"], button[type="reset"]').hide();
                    }

                    this.hideError();
                  //  console.log('lahat na',model,props);
                   
                    if(captchaContainer.length > 0){
                        var captchaResult = _self.validateCaptcha();
                        //console.log('validateCaptcha',captchaResult);
                        if(captchaResult.responseJSON.success){
                        return  _self.originalSaveForm(this,model,props);
                        }else{
                            this.showError('Captcha Failed. Please try again.','error',false);
                        
                            this.$savingForm.find('input[type="reset"], button[type="reset"]').show();
                            this.$savingForm.find('*[type=submit], *[type=reset]').attr('disabled', false);
                            
                            return false;
                        }
                    }else{
                        return _self.originalSaveForm(this,model,props);
                    }   
                    
                    
                }
			})
        }
        
         ,   originalSaveForm:function(formView,model,props){
                

                //Add validate method into the view.model
                

                var self = formView
                ,	options = self.selector ? {selector: self.selector} : {}

                // Returns the promise of the save action of the model
                ,	result = model.save(props || self.$savingForm.serializeObject(), _.extend({
                        wait: true

                    ,	forceUpdate: false

                        // Hides error messages, re enables buttons and triggers the save event
                        // if we are in a modal this also closes it
                    ,	success: function (model, response)
                        {
                            if (self.inModal && self.$containerModal)
                            {
                                self.$containerModal.removeClass('fade').modal('hide').data('bs.modal', null);
                            }

                            if (self.$savingForm.length)
                            {
                                self.hideError(self.$savingForm);
                                buttonSubmitDone(self.$savingForm);

                                model.trigger('save', model, response);
                            }
                            model.trigger('saveCompleted');
                        }

                        // Re enables all button and shows an error message
                    ,	error: function (model, response)
                        {
                            buttonSubmitDone(self.$savingForm);

                            if (response.responseText)
                            {
                                model.trigger('error', jQuery.parseJSON(response.responseText || 'null'));
                            }
                        }
                    }, options));

                if (result === false)
                {
                    self.$savingForm.find('input[type="reset"], button[type="reset"]').show();
                    self.$savingForm.find('*[type=submit], *[type=reset]').attr('disabled', false);
                }

                return result;
            }
        ,	validateCaptcha: function(){
            
                var skey = Configuration.get('kodellaApp.secretkey').trim();
                //console.log('grecaptcha',grecaptcha);
                var urlreq = Utils.getAbsoluteUrl(getExtensionAssetsPath('services/KDLGRecaptcha.Service.ss?sk='+skey+'&response='+grecaptcha.getResponse()));
                return $.getJSON({
                    url: urlreq, 
                        async: false
                },function(googleres){
                    //console.log('googleresext',googleres);
                    grecaptcha.reset();
                });
               
                    
                
                
        }
       
        
	}
	
});