// @module Kodella.KDSystemError.KDSystemError
define('Kodella.KDSystemError.KDSystemError.View'
,	[
		'Utils'
	,	'Backbone'
	,	'jQuery'
	,	'underscore'
	,	'Backbone.FormView'
	,	'SC.Configuration'
	]
,	function (
		Utils
	,	Backbone
	,	jQuery
	,	_
	,	BackboneFormView
	,	Configuration
	)
{
	'use strict';
	function buttonSubmitProgress (savingForm)
	{
		savingForm.find('[type="submit"]').each(function()
		{
			var element = jQuery(this);
			element.attr('disabled', true);
			element.data('default-text', jQuery.trim(element.text()));
			element.text(_('Processing...').translate());
		});
	}

	function buttonSubmitDone (savingForm)
	{
		savingForm.find('[type="submit"]').each(function()
		{
			var element = jQuery(this);
			element.attr('disabled', false);
			element.text(element.data('default-text'));
		});
	}
	// override default handler to uncheck radio buttons.
	var handler_radio = _.findWhere(Backbone.Stickit._handlers, {'selector': 'input[type="radio"]'});

	if (handler_radio)
	{
		handler_radio.update = function ($el, val)
		{
			if (val)
			{
				$el.filter('[value="'+val+'"]').prop('checked', true);
			}
			else
			{
				$el.prop('checked', false);
			}
		};
	}

	// @class Kodella.KDSystemError.KDSystemError.View @extends Backbone.View
	return {
		loadModule:function loadModule(options){
			console.log('extension bui',BackboneFormView);
			_.extend(BackboneFormView  || {},{
				saveForm :function (e, model, props)
				{
					e.preventDefault();
					
					//Add validate method into the view.model
					Backbone.Validation.bind(this);
		
					model = model || this.model;
		
					this.$savingForm = jQuery(e.target).closest('form');
					this.isSavingForm = true;
		
					if (this.$savingForm.length)
					{
						// and hides reset buttons
						this.$savingForm.find('input[type="reset"], button[type="reset"]').hide();
					}
		
					this.hideError();
		
					var self = this
					,	options = self.selector ? {selector: self.selector} : {}
		
					// Returns the promise of the save action of the model
					,	result = model.save(props || this.$savingForm.serializeObject(), _.extend({
							wait: true
		
						,	forceUpdate: false
		
							// Hides error messages, re enables buttons and triggers the save event
							// if we are in a modal this also closes it
						,	success: function (model, response)
							{
								if (self.inModal && self.$containerModal)
								{
									self.$containerModal.removeClass('fade').modal('hide').data('bs.modal', null);
								}
		
								if (self.$savingForm.length)
								{
									self.hideError(self.$savingForm);
									buttonSubmitDone(self.$savingForm);
		
									model.trigger('save', model, response);
								}
								model.trigger('saveCompleted');
							}
		
							// Re enables all button and shows an error message
						,	error: function (model, response)
							{
								buttonSubmitDone(self.$savingForm);
								
								if (response.responseText)
								{
									
									console.log('extension bui');
									if(Configuration.get('syserror.useOverrides')){
										_.map(Configuration.get('syserror.overrides',[]),function(customErrors){
										   
										   var nsErrorMessage = customErrors.nsError;
										   
										   if(response.responseJSON){
											   if(Utils.translate((response.responseJSON.errorMessage).trim()) == Utils.translate((nsErrorMessage).trim())){
												   response.responseJSON.errorMessage = Utils.translate(customErrors.customError);
			   
												   response.responseText = JSON.stringify(response.responseJSON);
											   }
										   }
									   });
									   
									  
								   }
									
									model.trigger('error', jQuery.parseJSON(response.responseText || 'null'));
								}
							}
						}, options));
		
					if (result === false)
					{
						this.$savingForm.find('input[type="reset"], button[type="reset"]').show();
						this.$savingForm.find('*[type=submit], *[type=reset]').attr('disabled', false);
					}
		
					return result;
				}
			})
		}
	}
	
});