// @module Kodella.KDLazyLoad.KDLazyLoad
define('Kodella.KDScrollFade.KDScrollFade.View'
,	[
		'Utils'
	,	'Backbone'
	,	'jQuery'
	,	'underscore'
	,	'SC.Configuration'
	]
,	function (
	
		Utils
	,	Backbone
	,	$
	,	_
	,	Configuration
	)
{
	'use strict';

	// @class Kodella.KDLazyLoad.KDLazyLoad.View @extends Backbone.View
	return {
		loadModule: function loadModule(){
			var useFadeScroll = Configuration.get('fadeScroll.useFadeScroll');
			
			//check if functionality is enabled in Configuration

			if(useFadeScroll){
				
				var configStyling = Configuration.get('fadeScroll.defaultStyle');
				var configDuration = Number(Configuration.get('fadeScroll.defaultDuration'));
				
				var selector = Configuration.get('fadeScroll.selectors',[]);

				
				if (selector.length > 0){
					var checkElement = selector.join();
					
					$(window).scroll(function() {
						
						var bottomScreen = $(window).scrollTop() + $(window).innerHeight();
						var topScreen = $(window).scrollTop();
						$(checkElement).each(function (){
							
							var topElement = $(this).offset().top;
							var bottomElement =  $(this).offset().top +  $(this).outerHeight();
								  
							if(!($(this).hasClass('kdl-animate-stop'))){
							
					  
								if ((bottomScreen > topElement) && (topScreen < bottomElement) ){
									// the element is visible, do something
									
									var styling = {
										opacity:configStyling
									}
									
									$(this).stop().animate(styling,configDuration);
									
								} else {
	
										// the element is not visible, do something else			
	
									$(this).stop().animate({opacity: "0"});
								}
							}
							//check if element has been hidden on the top of the screen then remove animation
							if(topScreen > bottomElement){
								$(this).addClass('kdl-animate-stop');
								$(this).removeClass('img-out-view');
							}
							//re attach animation if element has been hidden on the bottom of the screen
							if(topElement > bottomScreen){
								$(this).removeClass('kdl-animate-stop');
							}
						
						});
					});
					
					
				}
				
			}
							
		}
	}
});